<?php

namespace Drupal\d01_drupal_date_range_picker\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Render\Element\CompositeFormElementTrait;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * A form element for date range picker.
 *
 * Usage example:
 * @code
 * $form['daterangepicker'] = [
 *   '#title' => $this->t('Date picker'),
 *   '#type' => 'd01_drupal_date_range_picker',
 *   '#display_range' => TRUE,
 *   '#clear_label' => t('Clear'),
 *   '#required' => TRUE,
 *   '#min' => $start_date,
 *   '#max' => $end_date,
 *   '#default_value' => [
 *     'from' => new DrupalDateTime(),
 *     'until' => new DrupalDateTime(),
 *   ],
 *   '#js_settings' => [
 *     'format' => 'DD-MM-YYYY',
 *     'startOfWeek' => 'monday',
 *     'stickyMonths' => TRUE,
 *     'maxDays' => 85,
 *  ],
 * );
 * @endcode
 *
 * @RenderElement("d01_drupal_date_range_picker")
 */
class DateRangePicker extends FormElement {

  use CompositeFormElementTrait;

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#process' => [
        [$class, 'processDateRangePicker'],
      ],
      '#theme' => 'd01_drupal_date_range_picker',
      '#theme_wrappers' => [
        'form_element',
      ],
      '#display_range' => TRUE,
      '#clear_label' => t('Clear'),
      '#min' => FALSE,
      '#max' => FALSE,
      '#js_settings' => [],
      '#attributes' => [
        'class' => [
          'js-d01-drupal-drp',
        ],
      ],
      '#attached' => [
        'library' => [
          'd01_drupal_date_range_picker/date_range_picker',
        ],
        'drupalSettings' => [
          'd01_drupal_date_range_picker' => [],
        ],
      ],
    ];
  }

  /**
   * Processes a pickadate form element.
   */
  public static function processDateRangePicker(&$element, FormStateInterface $form_state, &$complete_form) {

    // Make sure we almost certain have a uniq id for every date picker.
    $unique_id = 'd01_drupal_drp_' . uniqid();

    $element['#tree'] = TRUE;
    $value = isset($element['#value']) && !empty($element['#value']) ? $element['#value'] : FALSE;

    // Set id and theming suggestions.
    $element['#attributes']['id'] = $unique_id;
    $element['#attributes']['data-twig-suggestions'] = ['d01_drupal_date_range_picker'];

    // Add the #js_settings keyed by id.
    $js_settings = is_array($element['#js_settings']) ? $element['#js_settings'] : [];
    $element['#attached']['drupalSettings']['d01_drupal_date_range_picker'][$unique_id] = $js_settings;

    // Set some default options and disable some stuff.
    $element['#attached']['drupalSettings']['d01_drupal_date_range_picker'][$unique_id]['inline'] = TRUE;
    $element['#attached']['drupalSettings']['d01_drupal_date_range_picker'][$unique_id]['alwaysOpen'] = TRUE;
    $element['#attached']['drupalSettings']['d01_drupal_date_range_picker'][$unique_id]['selectForward'] = TRUE;
    $element['#attached']['drupalSettings']['d01_drupal_date_range_picker'][$unique_id]['hoveringTooltip'] = FALSE;
    $element['#attached']['drupalSettings']['d01_drupal_date_range_picker'][$unique_id]['showShortcuts'] = FALSE;
    $element['#attached']['drupalSettings']['d01_drupal_date_range_picker'][$unique_id]['showTopbar'] = FALSE;

    // Set min date.
    $start_date = isset($element['#min']) && $element['#min'] ? $element['#min'] : FALSE;
    $start_timestamp = $start_date ? $start_date->getTimestamp() : FALSE;
    $element['#attached']['drupalSettings']['d01_drupal_date_range_picker'][$unique_id]['min'] = $start_timestamp;

    // Set max date.
    $end_date = isset($element['#max']) && $element['#max'] ? $element['#max'] : FALSE;
    $end_timestamp = $end_date ? $end_date->getTimestamp() : FALSE;
    $element['#attached']['drupalSettings']['d01_drupal_date_range_picker'][$unique_id]['max'] = $end_timestamp;

    // Check if we should display the selected range.
    $element['display_range'] = [];
    if ($element['#display_range']) {
      $element['display_range'] = [
        '#type' => 'd01_drupal_date_range_picker_range_display',
        '#attributes' => [
          'class' => [
            'js-d01-drupal-drp-display',
          ],
        ],
      ];
    }

    // Add date picker element.
    $element['calendar'] = [
      '#type' => 'd01_drupal_date_range_picker_calendar',
      '#attributes' => [
        'class' => [
          'js-d01-drupal-drp-calendar',
        ],
      ],
    ];

    $element['clear_range'] = [
      '#type' => 'd01_drupal_date_range_picker_clear_range',
      '#title' => $element['#clear_label'],
      '#attributes' => [
        'class' => [
          'js-d01-drupal-drp-clear',
        ],
      ],
    ];

    // Add hidden fields.
    $element['range'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => [
          'js-d01-drupal-drp-range',
        ],
      ],
    ];

    $from_value = isset($value['from']) && $value['from'] ? $value['from']->getTimestamp() : FALSE;
    $element['#attached']['drupalSettings']['d01_drupal_date_range_picker'][$unique_id]['from'] = $from_value;
    $element['from'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => [
          'js-d01-drupal-drp-from',
        ],
      ],
      '#value' => $from_value,
    ];

    $until_value = isset($value['until']) && $value['until'] ? $value['until']->getTimestamp() : FALSE;
    $element['#attached']['drupalSettings']['d01_drupal_date_range_picker'][$unique_id]['until'] = $until_value;
    $element['until'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => [
          'js-d01-drupal-drp-until',
        ],
      ],
      '#value' => $until_value,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input === FALSE) {
      $from = FALSE;
      if (isset($element['#default_value']) && isset($element['#default_value']['from'])) {
        $from = ($element['#default_value']['from'] instanceof DrupalDateTime) ? $element['#default_value']['from'] : FALSE;
      }

      $until = $from;
      if (isset($element['#default_value']) && isset($element['#default_value']['until'])) {
        $until = ($element['#default_value']['until'] instanceof DrupalDateTime) ? $element['#default_value']['until'] : FALSE;
      }

      return [
        'from' => $from,
        'until' => $until,
      ];
    }
    else {
      $from = FALSE;
      if (isset($input['from']) && !empty($input['from'])) {
        $from = DrupalDateTime::createFromTimestamp($input['from']);
      }

      $until = FALSE;
      if (isset($input['until']) && !empty($input['until'])) {
        $until = DrupalDateTime::createFromTimestamp($input['until']);
      }

      return [
        'from' => $from,
        'until' => $until,
      ];
    }
  }

}
