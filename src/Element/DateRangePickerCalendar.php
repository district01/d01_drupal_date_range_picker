<?php

namespace Drupal\d01_drupal_date_range_picker\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * A calendar render element for d01_drupal_date_range_picker.
 *
 * @RenderElement("d01_drupal_date_range_picker_calendar")
 */
class DateRangePickerCalendar extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'd01_drupal_date_range_picker_calendar',
      '#attributes' => [],
    ];
  }

}
