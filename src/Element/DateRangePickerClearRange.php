<?php

namespace Drupal\d01_drupal_date_range_picker\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * A clear render element for d01_drupal_date_range_picker.
 *
 * @RenderElement("d01_drupal_date_range_picker_clear_range")
 */
class DateRangePickerClearRange extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
      '#theme' => 'd01_drupal_date_range_picker_clear_range',
      '#attributes' => [],
      '#title' => [],
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
    ];
  }

  /**
   * Prerender function for Slick element.
   */
  public static function preRenderElement($element) {
    $element['title'] = ['#markup' => $element['#title']];
    return $element;
  }

}
