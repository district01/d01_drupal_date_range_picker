

/**
 * BoardGrid.
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.d01_drupal_date_range_picker = {
    attach: function (context, drupalSettings) {
      var componentSettings = drupalSettings.d01_drupal_date_range_picker || {};
      var wrapperClass = '.js-d01-drupal-drp';
      $(wrapperClass, context).once('initializeDateRangePickers').each(function(i, obj) {
        var el = $(this);
        var elId = (el.attr('id')) ? el.attr('id') : false;
        if (elId) {
          var options = componentSettings[elId] || {};
          new D01DrupalDateRangePicker(
            el,
            options
          ).init();
        }
      });
    }
  };
})(jQuery, Drupal);
