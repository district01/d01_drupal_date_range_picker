(function ($) {

  'use strict';

  /**
   * jQuery object
   * @external jQuery
   * @see {@link http://api.jquery.com/jQuery/}
   */

  /**
   * D01DrupalDateRangePicker
   *
   * @param {object} el
   *    a jQuery Dom element.
   * @param {object} dateRangeOptions
   *    an object containing all settings.
   * @constructor
   */
  var D01DrupalDateRangePicker = function (el, dateRangeOptions) {
    var that = this;
    that.el = el;
    that.dateRangeOptions = dateRangeOptions || {};

    that.rangeInput = that.el.find('.js-d01-drupal-drp-range');
    that.fromInput = that.el.find('.js-d01-drupal-drp-from');
    that.untilInput = that.el.find('.js-d01-drupal-drp-until');
    that.calendar = that.el.find('.js-d01-drupal-drp-calendar');
    that.clear = that.el.find('.js-d01-drupal-drp-clear');
    that.display = that.el.find('.js-d01-drupal-drp-display');

    // Fallback to default settings of plugin.
    that.dateFormat = that.dateRangeOptions['format'] || 'YYYY-MM-DD';
    that.seperator = that.dateRangeOptions['separator'] || ' to ';

    var min_timestamp = that.dateRangeOptions['min'] || false;
    var max_timestamp = that.dateRangeOptions['max'] || false;

    that.dateRangeOptions['startDate'] = min_timestamp ? moment.unix(min_timestamp).format(that.dateFormat) : false;
    that.dateRangeOptions['endDate'] = max_timestamp ? moment.unix(max_timestamp).format(that.dateFormat) : false;

    that.dateRangeOptions['container'] = that.calendar;
  };

  /**
   * initializePickers().
   */
  D01DrupalDateRangePicker.prototype.init = function () {
    let that = this;

    // Setup date picker and actions.
    that.rangeInput.dateRangePicker(that.dateRangeOptions)
      .bind('datepicker-first-date-selected', function(event, obj) {
        var start = moment(obj.date1);
        var end = moment(obj.date1);

        // On selection of 1 date start and end are the same.
        that.fromInput.val(start.unix());
        that.untilInput.val(end.unix());

        // Display the selection.
        that.display.html(start.format(that.dateFormat));
      })
      .bind('datepicker-change',function(event,obj) {
        var start = moment(obj.date1);
        var end = moment(obj.date2);

        that.fromInput.val(start.unix());
        that.untilInput.val(end.unix());

        that.display.html(start.format(that.dateFormat) + that.seperator + end.format(that.dateFormat));
      });

    // Set default values.
    if (that.dateRangeOptions['from'] && that.dateRangeOptions['until']) {
      var from = moment.unix(that.dateRangeOptions['from']).format(that.dateFormat);
      var until = moment.unix(that.dateRangeOptions['until']).format(that.dateFormat);

      var display = from === until ? from : from + that.seperator + until;

      that.rangeInput.data('dateRangePicker').setDateRange(from, until);
      that.display.html(display);
    }

    if (that.dateRangeOptions['from'] && !that.dateRangeOptions['until']) {
      var from = moment.unix(that.dateRangeOptions['from']).format(that.dateFormat);
      var until = moment.unix(that.dateRangeOptions['from']).format(that.dateFormat);

      that.rangeInput.data('dateRangePicker').setDateRange(from, until);
      that.display.html(from);
    }

    // Set up clear action.
    that.clear.on('click', function(e){
      that.rangeInput.data('dateRangePicker').clear();
      that.fromInput.val('');
      that.untilInput.val('');
      that.display.html('');
      e.preventDefault();
    });
  };

  /**
   * @type {D01DrupalDateRangePicker}
   */
  window.D01DrupalDateRangePicker = D01DrupalDateRangePicker;

})(jQuery);
